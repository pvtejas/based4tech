## Development

From the root directory, run `pip install -r requirements.txt` to install the package (and all dependencies) in editable mode.  
Use `gunicorn -w 2 'flaskr:create_app()'` to run app. Increase the number of workers using the `-w` argument if desired. The package will be updated as you edit files.

## Production
Run `python -m build --wheel` to generate the wheel, and install the wheel (found in `dist/`) in the production environment.  
Use `gunicorn -w 2 'flaskr:create_app()'` to run app. Increase the number of workers using the `-w` argument if desired. To update package, you will need to install a new wheel.

## Initializing database
The first time you install the app in each environment, you need to initialize database using `flask --app flaskr init-db`. This only needs to be run once per environment, and **will delete existing database if run again**.

## Config file

The config file is located at `<python_environment>/var/flaskr-instance/config.py` in production, and in `instance/` in development. The instance folder is created when the database is initialized.

### Secret Key
Every website with login needs a secret key to hash passwords with.
The config file must contain a line `SECRET_KEY = '<secret_key>`, which must be randomly generated.  
Suggested way of generating the key is `python -c 'import secrets; print(secrets.token_hex())'`, which returns a hexadecimal string with length 64. You may choose to randomly generate a key using a different method, but ensure that it is resistant to brute-force attacks.

### Registration
Since this blog is meant to be updated by a limited number of people, registration is forbidden (403) by default. In addition, registration (/auth/register) and login (/auth/login) URLs are not hyperlinked anywhere. Registration can be opened by including `REGISTER = True`, and is closed by default.

### Name
The default app name is "Flaskr", and it is visible on the header bar as well as the page title. Including a line `NAME = '<name>'` in the config file replaces "Flaskr" with your chosen name.

### Static folder
The default static folder is the one included in the repository. You can use a separate static folder to use your own assets by including a line `STATIC_FOLDER = '<absolute/path/to/folder>'` in the config file.
