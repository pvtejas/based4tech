Merge requests and issues are welcome.

Merge requests must be targeted at issues, with each merge request including a line (or multiple) "Closes #<issue-number>" in it. If a merge request doesn't close an issue, consider raising an issue before submitting the MR.
